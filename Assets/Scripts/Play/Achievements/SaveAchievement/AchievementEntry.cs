﻿using System;

namespace Game
{
    // Author : Carol-Ann Collin
    [Serializable]
    public class AchievementEntry
    {
        public int level;
        public AchievementType achievementType;
        public string unlockedTime;

        public AchievementEntry(int level, AchievementType achievementType, string unlockedTime)
        {
            this.level = level;
            this.achievementType = achievementType;
            this.unlockedTime = unlockedTime;
        }

        protected bool Equals(AchievementEntry other)
        {
            return level == other.level && achievementType == other.achievementType;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = level;
                hashCode = (hashCode * 397) ^ (int) achievementType;
                hashCode = (hashCode * 397) ^ (unlockedTime != null ? unlockedTime.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}