﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class AchievementRecorder : MonoBehaviour
    {
        private Main main;
        private AchievementFileWriter achievementFileWriter;
        private AchievementEventChannel achievementEventChannel;

        private void Awake()
        {
            main = Finder.Main;
            achievementFileWriter = Finder.AchievementFileWriter;
            achievementEventChannel = Finder.AchievementEventChannel;
            achievementEventChannel.OnAchievementUnlocked += OnAchievementUnlocked;
        }

        private void OnDestroy()
        {
            achievementEventChannel.OnAchievementUnlocked -= OnAchievementUnlocked;
        }

        private void OnAchievementUnlocked(AchievementType achievementType, DateTime unlockedTime)
        {
            int level = main.LevelToLoad;
            if (achievementType == AchievementType.Level3CompletedOnTime)
            {
                level -= 1;
            }
            achievementFileWriter.CreateAchievementEntry(level, achievementType, unlockedTime);
        }
    }
}