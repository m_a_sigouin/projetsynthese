﻿using UnityEngine;

namespace Game
{
    public class BeeMover : MonoBehaviour
    {
        [SerializeField] private float speed;
        [SerializeField] private float rotationSpeed;
        
        
        private const float TOLERANCE = 0.05f;
        private Vector3 currentPosition => transform.position;
        public Vector3 CurrentDestination { get; set; }

        public bool IsAtDestination => Vector2.Distance(transform.position, CurrentDestination) <= TOLERANCE;
        
        public void Move()
        {
            Vector3 movement = CurrentDestination - currentPosition;
            movement.Normalize();
            
            transform.up = Vector3.Lerp(transform.up, movement, Time.deltaTime * rotationSpeed);
            
            movement *= speed * Time.deltaTime;
            transform.Translate(movement);
        }
    }
}