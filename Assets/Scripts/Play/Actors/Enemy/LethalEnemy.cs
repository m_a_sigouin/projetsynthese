﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class LethalEnemy : MovingCharacters
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            var otherGameObject = other.gameObject;
            IHurtable objectToHurt = otherGameObject.GetComponent<IHurtable>();
            if (objectToHurt != null && (enemyMover == null || !enemyMover.IsPaused) && flag.PlayerShouldUpdate)
            {
                objectToHurt.Hurt();
                if (CompareTag(Tags.Vehicule))
                {
                    AchievementPublisher.UnlockAchievement(AchievementType.PlayerHitByVehicle);
                }
            }
        }
    }
}