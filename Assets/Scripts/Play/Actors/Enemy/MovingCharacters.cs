﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class MovingCharacters : Recyclable, IHearable
    {
        [Header("Moving characters properties")]
        [SerializeField][Min(0.1f)] private Vector2 size;
        [SerializeField] private bool isColliderCircle;
        [SerializeField] private float delay = 5f;
        
        [Header("Light")]
        [SerializeField] private Sprite lightSprite;
        [SerializeField][Range(0, 1)] private float percentageLight = 0.5f;

        protected FlagUpdates flag;
        protected PathMover enemyMover;
        private AchievementPublisher achievementPublisher;
        private LightingSpriteRenderer2D lightSpriteRenderer;
        private AtmosphereSound atmosphereSound;
        private Despawner despawner;
        private Collider2D collider2D;
        
        private bool isHeard;
        private bool shouldChangeState = true;
        
        protected AchievementPublisher AchievementPublisher => achievementPublisher;
        public Vector2 Size => size;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            enemyMover = GetComponent<PathMover>();
            achievementPublisher = Finder.AchievementPublisher;

            if (isColliderCircle)
            {
                CreateCircleCollider2D();
            }
            else
            {
                CreateBoxCollider2D();
            }
            CreateLightSprite();
            GetAtmosphereSound();
        }

        protected virtual void Update()
        {
            UpdateCharacter();
        }

        protected void UpdateCharacter()
        {
            if (!flag.PlayerShouldUpdate)
            {
                collider2D.isTrigger = true;
            }
            else
            {
                collider2D.isTrigger = false;
            }
            if (flag.EnemyShouldUpdate)
            {
                if (enemyMover != null)
                {
                    enemyMover.Move();
                }

                if (atmosphereSound.IsPlaying && shouldChangeState)
                {
                    lightSpriteRenderer.DotweenTo(size * percentageLight, delay);
                    if (isActiveAndEnabled)
                    {
                        StartCoroutine(shouldChangeState.ChangeState(delay));
                    }
                }
                else if (!atmosphereSound.IsPlaying && shouldChangeState)
                {
                    lightSpriteRenderer.DotweenTo(Vector2.zero, delay);
                    if (isActiveAndEnabled)
                    {
                        StartCoroutine(shouldChangeState.ChangeState(delay));
                    }
                }
            }
        }

        private void CreateBoxCollider2D()
        {
            BoxCollider2D collider = gameObject.AddComponent<BoxCollider2D>();
            collider.size = size;
            collider2D = collider;
        }

        private void CreateCircleCollider2D()
        {
            CircleCollider2D collider = gameObject.AddComponent<CircleCollider2D>();
            collider.radius = size.x + size.y / 2;
            collider2D = collider;
        }

        private void CreateLightSprite()
        {
            lightSpriteRenderer = gameObject.AddComponent<LightingSpriteRenderer2D>();
            lightSpriteRenderer.sprite = lightSprite;
            lightSpriteRenderer.offsetScale.x = 0f;
            lightSpriteRenderer.offsetScale.y = 0f;
        }

        private void GetAtmosphereSound()
        {
            atmosphereSound = gameObject.GetComponent<AtmosphereSound>();
        }

        public void Hear()
        {
            IEnumerator IsHeardCoroutine()
            {
                isHeard = true;
                lightSpriteRenderer.DotweenTo(size * percentageLight, delay);
                yield return new WaitForSeconds(delay);
                if (!atmosphereSound.IsPlaying)
                {
                    lightSpriteRenderer.DotweenTo(Vector2.zero, delay);
                }
                isHeard = false;
            }

            if (!isHeard)
            {
                StartCoroutine(IsHeardCoroutine());
            }
        }

        // Author : Vivianne Lord
        // Appelée lorsqu'un ennemi est créé via un Spawner
        public void OnSpawn(List<Waypoint> path, bool repeat, bool shouldDespawn)
        {
            OnSpawnPath(path, repeat);
            OnSpawnDespawn(shouldDespawn);
        }

        // Author : Vivianne Lord
        private void OnSpawnPath(List<Waypoint> path, bool repeat)
        {
            if (enemyMover != null)
            {
                enemyMover.Waypoints.Copy(path);
                enemyMover.Repeat = repeat;
                enemyMover.SetPath();
            }
        }

        private void OnSpawnDespawn(bool shouldDespawn)
        {
            if (shouldDespawn)
            {
                if (despawner == null)
                {
                    despawner = gameObject.AddComponent<Despawner>();
                }

                despawner.enabled = true;
            }
            else
            {
                if (despawner != null)
                {
                    despawner.enabled = false;
                }
            }
        }
    }
}