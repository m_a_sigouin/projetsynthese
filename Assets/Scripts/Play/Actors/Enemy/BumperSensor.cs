﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class BumperSensor : MonoBehaviour
    {
        private FlagUpdates flag;
        private BumperCompute bumperCompute;
        
        private void Awake()
        {
            bumperCompute = GetComponent<BumperCompute>();
            flag = Finder.FlagUpdates;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var otherGameObject = other.gameObject;
            var playerMover = otherGameObject.GetComponent<PlayerMover>();
            if (playerMover != null && flag.PlayerShouldUpdate)
            {
                bumperCompute.Bump(playerMover);
            }
        }
    }
}