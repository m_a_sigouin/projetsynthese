﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class BrakeAssist : MonoBehaviour
    {
        [Range(1f, 100f)] 
        [SerializeField] private float percent = 20f;
        
        private PathMover pathMover;
        private bool canBePaused = true;

        public bool Break
        {
            set
            {
                if (canBePaused)
                {
                    pathMover.Pause(value);
                }
            }
        }

        private void Awake()
        {
            pathMover = GetComponentInParent<PathMover>();
            AdjustColliderForAssist();
        }

        private void OnDisable()
        {
            canBePaused = true;
            Break = false;
        }

        private void AdjustColliderForAssist()
        {
            MovingCharacters movingCharacters = GetComponentInParent<MovingCharacters>();
            Vector2 size = movingCharacters.Size;
            size.y *= (percent / 100);
            BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();
            boxCollider.size = size;
            Vector2 offset = boxCollider.offset;
            offset.y = (boxCollider.size.y / 2) + (movingCharacters.Size.y / 2);
            boxCollider.offset = offset;
        }
    }
}