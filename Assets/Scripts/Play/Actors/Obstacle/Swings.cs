﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class Swings : MonoBehaviour
    {
        [SerializeField] private RandomEnemySpawner spawner;
        
        [Header("Swing sprites")]
        [SerializeField] private Sprite swingForward;
        [SerializeField] private Sprite swingBackward;
        [SerializeField] private Sprite notSwinging;
        
        private FlagUpdates flag;
        private SpriteRenderer renderer;
        private Child child;

        private int numberOfSwingsUntilSpawn;
        private int swingSpriteRefreshCount;
        private bool childStillExists;
        private bool swingCanRefresh;
        private bool isDelayActive;

        public Child Child
        {
            set
            {
                child = value;
                child.OnChildDespawn += OnChildDespawn;
            }
        }

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            renderer = GetComponent<SpriteRenderer>();
            numberOfSwingsUntilSpawn = Random.Range(2, 5);
            swingSpriteRefreshCount = 0;
        }

        private void Update()
        {
            if (flag.SpawnersShouldUpdate)
            {
                if (!childStillExists)
                {
                    if (!isDelayActive)
                    {
                        StartCoroutine(SwingSpriteRefreshDelay());
                    }

                    if (swingSpriteRefreshCount % 2 == 0 && swingCanRefresh)
                    {
                        swingCanRefresh = false;
                        renderer.sprite = swingBackward;
                    }
                    else if (swingCanRefresh)
                    {
                        swingCanRefresh = false;
                        renderer.sprite = swingForward;
                        numberOfSwingsUntilSpawn--;
                    }

                    if (spawner != null && numberOfSwingsUntilSpawn <= 0)
                    {
                        spawner.ShouldSpawn = true;
                        childStillExists = true;
                    }
                }
                else
                {
                    if (renderer.sprite != notSwinging)
                    {
                        renderer.sprite = notSwinging;
                    }
                }
            }
        }

        private void OnDestroy()
        {
            if (isDelayActive)
            {
                StopCoroutine(SwingSpriteRefreshDelay());
            }

            if (child != null)
            {
                child.OnChildDespawn -= OnChildDespawn;
            }
        }

        private IEnumerator SwingSpriteRefreshDelay()
        {
            isDelayActive = true;
            
            while (isDelayActive && flag.SpawnersShouldUpdate)
            {
                if (!childStillExists)
                {
                    yield return new WaitForSeconds(1);
                    swingSpriteRefreshCount++;
                    swingCanRefresh = true;
                }
                else
                {
                    isDelayActive = false;
                }
            }

            // Traitement à effectuer si la balançoire se fait désactiver par le FlagUpdate.
            if (isDelayActive)
            {
                isDelayActive = false;
            }
        }

        private void OnChildDespawn()
        {
            child = null;
            childStillExists = false;
            numberOfSwingsUntilSpawn = Random.Range(2, 5);
        }
    }
}