﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault and Marc-Antoine Sigouin
    public class PlayerConcentration : MonoBehaviour
    {
        [Header("Light settings")]
        [SerializeField][Range(0.01f, 1f)] private float speed = 0.1f;
        [SerializeField] private float maxLightSize = 15;
        [SerializeField][Min(0)] private float minLightSize = 1;
        
        [Header("Concentration settings")]
        [SerializeField][Range(1, 10)] private float concentrationLevel = 10;

        private PlayerLight playerLight;
        private HUDConcentrationLevel concentrationBar;

        public bool IsConcentrationActive { get; set; }
        public bool IsConcentrationDeactivated => concentrationLevel <= 0;
        
        private bool IsPlayerLightSizeMaximized => playerLight.LightSize >= maxLightSize;
        private bool IsPlayerLightSizeMinimized => playerLight.LightSize <= minLightSize;
        
        private void Awake()
        {
            playerLight = GetComponent<PlayerLight>();
            concentrationBar = Finder.HUDConcentrationLevel;
            concentrationBar.ConcentrationLevel = concentrationLevel;
            Finder.ConcentrationEventChannel.Publish(false);
        }

        private void Update()
        {
            if (!IsConcentrationDeactivated && IsConcentrationActive)
            {
                if (!IsPlayerLightSizeMaximized)
                {
                    MakeLightSizeBigger();
                }
                
                UpdateConcentrationLevel();
            }
            else if (!IsPlayerLightSizeMinimized)
            {
                MakeLightSizeSmaller();
            }
        }

        // Author : Marc-Antoine Sigouin
        private void UpdateConcentrationLevel()
        {
            concentrationBar.ConcentrationLevel = (concentrationLevel -= Time.deltaTime);

            if (concentrationLevel <= 0)
            {
                Finder.ConcentrationEventChannel.Publish(true);
                Finder.ConcentrationEventChannel.PublishDeactivation();
            }
        }
        
        private void MakeLightSizeBigger()
        {
            playerLight.LightSize += speed;
        }
        
        private void MakeLightSizeSmaller()
        {
            playerLight.LightSize -= speed;
        }
    }
}