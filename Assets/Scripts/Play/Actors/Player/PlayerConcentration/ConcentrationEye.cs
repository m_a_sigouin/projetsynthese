﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class ConcentrationEye : MonoBehaviour
    {
        [SerializeField] private Sprite openedEye;
        [SerializeField] private Sprite closedEye;

        private Image eyeImg;
        private ConcentrationEventChannel eventChannel;

        private void Awake()
        {
            eventChannel = Finder.ConcentrationEventChannel;
            eventChannel.OnConcentrationStarted += OpenEye;
            eventChannel.OnConcentrationStopped += CloseEye;

            eyeImg = GetComponent<Image>();
            CloseEye();
        }

        private void OnDestroy()
        {
            eventChannel.OnConcentrationStarted -= OpenEye;
            eventChannel.OnConcentrationStopped -= CloseEye;
        }

        private void OpenEye()
        {
            eyeImg.sprite = openedEye;
        }

        private void CloseEye()
        {
            eyeImg.sprite = closedEye;
        }
    }
}