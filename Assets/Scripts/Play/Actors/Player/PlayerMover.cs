﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] private float speed = 2f;
        [SerializeField] private float inactiveTimeAfterBumped = 0.5f;
        private const int DIVIDER = 50;

        private new Rigidbody2D rigidbody2D;

        private bool isBumped;

        public float Speed
        {
            get => speed;

            set => speed = value;
        }

        private void Awake()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public void Move(Vector2 movement)
        {
            if (!isBumped)
            {
                rigidbody2D.velocity = movement * (speed * Time.deltaTime);
            }
        }

        // Auhtor : Joanie Labbé
        public void Bump(Vector3 force, float enemySpeed)
        {
            IEnumerator BumpedRoutine()
            {
                isBumped = true;
                rigidbody2D.velocity = force * (speed/DIVIDER * enemySpeed);
                yield return new WaitForSeconds(inactiveTimeAfterBumped);
                isBumped = false;
            }

            if (!isBumped)
            {
                StartCoroutine(BumpedRoutine());
            }
        }
    }
}