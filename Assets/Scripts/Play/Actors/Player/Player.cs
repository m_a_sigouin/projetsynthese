﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author: Carol-Ann Collin
    [Findable(Tags.Player)]
    public class Player : Recyclable, IHurtable
    {
        private FlagUpdates flag;
        private InputActions.GameActions gameInputs;
        private PlayerSonar playerSonar;
        private PlayerMover playerMover;
        private PlayerConcentration playerConcentration;
        private PlayerSteps playerSteps;
        private Harness harness;
        private InteractableSensor interactableSensor;
        private InteractionLight interactionLight;
        private SpriteChanger spriteChanger;
        
        private float startSpeed;
        private bool isAlreadyHurt = false;

        private bool IsInteractionPossible => interactableSensor.InteractableObjects.Count > 0;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            gameInputs = Finder.Inputs.Actions.Game;
            playerSonar = GetComponent<PlayerSonar>();
            playerMover = GetComponent<PlayerMover>();
            playerConcentration = GetComponent<PlayerConcentration>();
            playerSteps = GetComponentInChildren<PlayerSteps>();
            harness = GetComponentInChildren<Harness>();
            interactableSensor = GetComponentInChildren<InteractableSensor>();
            interactionLight = GetComponentInChildren<InteractionLight>();
            spriteChanger = GetComponent<SpriteChanger>();

            startSpeed = playerMover.Speed;
        }

        private void Update()
        {
            if (flag.PlayerShouldUpdate)
            {
                PlayerUpdate();
            }
        }

        private void FixedUpdate()
        {
            var moveValue = gameInputs.Move.ReadValue<Vector2>();
            playerMover.Move(moveValue);
            VerifyIfPlayerIsWalking(moveValue);
            
            if (moveValue != Vector2.zero)
            {
                Rotate(moveValue);
            }
        }

        private void PlayerUpdate()
        {
            
            if (IsInteractionPossible)
            {
                interactionLight.Open();
            }
            else if (interactionLight.isActiveAndEnabled)
            {
                interactionLight.Close();
            }

            if (gameInputs.Echolocation.triggered)
            {
                playerSonar.SpawnSonar();
            }
            
            // InteractionStart est un événement press only sur la touche de l'interaction
            if (gameInputs.InteractionStart.triggered && IsInteractionPossible)
            {
                interactableSensor.InteractableObjects.ForEach(obj => obj.StartInteraction(this));
            }
            
            // InteractionStart est un événement release only sur la touche de l'intereaction
            if (gameInputs.InteractionEnd.triggered && IsInteractionPossible)
            {
                interactableSensor.InteractableObjects.ForEach(obj => obj.EndInteraction());
            }
            
            
            // InteractionStart est un événement press only sur la touche de la concentration
            if (gameInputs.ConcentrationModeStart.triggered)
            {
                playerConcentration.IsConcentrationActive = true;
                if (!playerConcentration.IsConcentrationDeactivated)
                {
                    Finder.ConcentrationEventChannel.PublishActivation();
                }
                TogglePlayerSpeedOnConcentration(true);
            }
            
            // InteractionStart est un événement release only sur la touche de la concentration
            if (gameInputs.ConcentrationModeEnd.triggered)
            {
                playerConcentration.IsConcentrationActive = false;
                Finder.ConcentrationEventChannel.PublishDeactivation();
                TogglePlayerSpeedOnConcentration(false);
            }
        }
        
        // Author : Marc-Antoine Sigouin
        private void Rotate (Vector2 direction)
        {
            spriteChanger.DetermineSpriteToUse(direction);

            if (harness != null)
            {
                harness.Rotate(direction);
            }
        }
        
        // Author : Marc-Antoine Sigouin
        private void TogglePlayerSpeedOnConcentration (bool isEnabled)
        {
            if (isEnabled && !playerConcentration.IsConcentrationDeactivated)
            {
                if (playerMover.Speed - startSpeed * 0.26f >= 0.1f)
                {
                    playerMover.Speed = startSpeed * 0.25f;
                    if (harness != null)
                    {
                        harness.ToggleDogLightSource(false);
                    }
                }
            }
            else
            {
                if (playerMover.Speed - startSpeed <= -0.1f)
                {
                    playerMover.Speed = startSpeed;
                    if (harness != null)
                    {
                        harness.ToggleDogLightSource(true);
                    }
                }
            }
        }

        private void VerifyIfPlayerIsWalking(Vector2 moveValue)
        {
            if (moveValue != new Vector2(0, 0))
            {
                playerSteps.IsWalking = true;
            }
            else
            {
                playerSteps.IsWalking = false;
            }
        }

        public void Hurt()
        {
            if (!isAlreadyHurt)
            {
                Finder.LevelFailedEventChannel.Publish();
                Finder.ObjectivesEventChannel.PublishReseted();
                Finder.ChecklistEventChannel.PublishReset();
                isAlreadyHurt = true;
            }
        }

        public void RemoveOldPlayer()
        {
            Recycle();
        }
    }
}