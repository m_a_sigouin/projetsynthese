﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class InteractionLight : MonoBehaviour
    {
        private LightingSource2D lightingSource2D;

        private void Awake()
        {
            lightingSource2D = GetComponent<LightingSource2D>();
            lightingSource2D.enabled = false;
        }

        public void Open()
        {
            lightingSource2D.enabled = true;
        }

        public void Close()
        {
            lightingSource2D.enabled = false;
        }
    }
}