﻿namespace Game
{
    // Author : Joanie Labbé
    public interface IInteractable
    {
        void StartInteraction(Player sender);
        void EndInteraction();
    }
}