﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class Pickable : Recyclable, IInteractable
    {
        [SerializeField] private InventoryItem inventoryItem;
        [SerializeField] private AudioClip clip;

        private const string CARD = "Card";
        private const string COFFEE = "Coffee";
        private const string KEY = "Key";
        
        private AchievementPublisher achievementPublisher;
        private SoundPlayer soundPlayer;

        private void Awake()
        {
            achievementPublisher = Finder.AchievementPublisher;
            if (clip != null)
            {
                soundPlayer = Finder.SoundPlayer;
            }
        }

        public void StartInteraction(Player sender)
        {
            Finder.Inventory.IncreaseQuantity(inventoryItem);
            switch (inventoryItem.Tag)
            {
                case COFFEE:
                    soundPlayer.PlaySound(clip);
                    achievementPublisher.UnlockAchievement(AchievementType.CoffeeTaken);
                    break;
                case CARD:
                    soundPlayer.PlaySound(clip);
                    break;
            }

            if (Finder.Main.LevelToLoad == 5 && inventoryItem.Tag == KEY)
            {
                achievementPublisher.UnlockAchievement(AchievementType.Level5KeysPickedUp);
            }
            Recycle();
        }

        public void EndInteraction()
        {
            // Rien à faire
        }
    }
}