﻿using System;
using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class LevelSelectionMenu : MonoBehaviour, IMenu
    {
        [SerializeField] private Sprite imageWhenLevelNotUnlocked;

        private const int LOCKED_INDEX = 0;
        private const int UNLOCKED_INDEX = 1;
        
        private Main main;
        private InputActions.MenuActions menuInputs;
        private MenuNavigator menuNavigator;
        private LevelSelectionLanguageModel languageModel;

        private Button firstLevelButton;
        private Button secondLevelButton;
        private Button thirdLevelButton;
        private Button fourthLevelButton;
        private Button fifthLevelButton;
        private Button backButton;
        
        private Image secondLevelImage;
        private Image thirdLevelImage;
        private Image fourthLevelImage;
        private Image fifthLevelImage;

        private TextMeshProUGUI[] levelSelectLabels;
        private TextMeshProUGUI firstLevelTitle;
        private TextMeshProUGUI secondLevelTitle;
        private TextMeshProUGUI thirdLevelTitle;
        private TextMeshProUGUI fourthLevelTitle;
        private TextMeshProUGUI fifthLevelTitle;
        private TextMeshProUGUI firstLevelDescription;
        private TextMeshProUGUI secondLevelDescription;
        private TextMeshProUGUI thirdLevelDescription;
        private TextMeshProUGUI fourthLevelDescription;
        private TextMeshProUGUI fifthLevelDescription;
        private TextMeshProUGUI[] backButtonLabels;
        
        private Canvas levelSelectionCanvas;

        private void Awake()
        {
            main = Finder.Main;
            menuInputs = Finder.Inputs.Actions.Menu;
            menuNavigator = Finder.MenuNavigator;
            levelSelectionCanvas = GetComponentInChildren<Canvas>();
            languageModel = new LevelSelectionLanguageModel();
            
            SetActiveLanguage();
            FindLabelInView();
            FindButtonsInView();
            UpdateFromUnlockLevel();
        }

        private void Start()
        {
            menuInputs.Enable();
            menuNavigator.Push(this);
        }
        
        private void OnEnable()
        {
            firstLevelButton.onClick.AddListener(SelectFirstLevel);
            secondLevelButton.onClick.AddListener(SelectSecondLevel);
            thirdLevelButton.onClick.AddListener(SelectThirdLevel);
            fourthLevelButton.onClick.AddListener(SelectFourthLevel);
            fifthLevelButton.onClick.AddListener(SelectFifthLevel);
            backButton.onClick.AddListener(ReturnToMainMenu);
        }

        private void OnDisable()
        {
            firstLevelButton.onClick.RemoveListener(SelectFirstLevel);
            secondLevelButton.onClick.RemoveListener(SelectSecondLevel);
            thirdLevelButton.onClick.RemoveListener(SelectThirdLevel);
            fourthLevelButton.onClick.RemoveListener(SelectFourthLevel);
            fifthLevelButton.onClick.RemoveListener(SelectFifthLevel);
            backButton.onClick.RemoveListener(ReturnToMainMenu);
        }

        private void Update()
        {
            SelectButtonIfNoneSelected();
        }

        private void FindLabelInView()
        {
            levelSelectLabels = GetComponentsInChildren<TextMeshProUGUI>();
            foreach (var t in levelSelectLabels)
            {
                t.text = languageModel.Labels[0];
            }
        }
        
        private void FindButtonsInView()
        {
            var buttons = GetComponentsInChildren<Button>();

            firstLevelButton = buttons.WithName(GameObjects.Level1);
            secondLevelButton = buttons.WithName(GameObjects.Level2);
            thirdLevelButton = buttons.WithName(GameObjects.Level3);
            fourthLevelButton = buttons.WithName(GameObjects.Level4);
            fifthLevelButton = buttons.WithName(GameObjects.Level5);
            backButton = buttons.WithName(GameObjects.BackToMain);
            
            FindButtonsImages();

            var firstLevelLabels = firstLevelButton.GetComponentsInChildren<TextMeshProUGUI>();
            var secondLevelLabels = secondLevelButton.GetComponentsInChildren<TextMeshProUGUI>();
            var thirdLevelLabels = thirdLevelButton.GetComponentsInChildren<TextMeshProUGUI>();
            var fourthLevelLabels = fourthLevelButton.GetComponentsInChildren<TextMeshProUGUI>();
            var fifthLevelLabels = fifthLevelButton.GetComponentsInChildren<TextMeshProUGUI>();
            
            SetTitlesToButtons(firstLevelLabels, secondLevelLabels, thirdLevelLabels, fourthLevelLabels, fifthLevelLabels);
            SetInitialDescriptions(firstLevelLabels, secondLevelLabels, thirdLevelLabels, fourthLevelLabels, fifthLevelLabels);

            backButtonLabels = backButton.GetComponentsInChildren<TextMeshProUGUI>();
            for (int g = 0; g < backButtonLabels.Length; g++)
            {
                backButtonLabels[g].text = languageModel.Buttons[0];
            }
        }

        private void FindButtonsImages()
        {
            secondLevelImage = secondLevelButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageLevel2);
            thirdLevelImage = thirdLevelButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageLevel3);
            fourthLevelImage = fourthLevelButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageLevel4);
            fifthLevelImage = fifthLevelButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageLevel5);
        }

        private void SetInitialDescriptions(TextMeshProUGUI[] firstLevelLabels, 
                                            TextMeshProUGUI[] secondLevelLabels,
                                            TextMeshProUGUI[] thirdLevelLabels, 
                                            TextMeshProUGUI[] fourthLevelLabels, 
                                            TextMeshProUGUI[] fifthLevelLabels)
        {
            firstLevelDescription = firstLevelLabels.WithName(GameObjects.DescriptionLevel1);
            secondLevelDescription = secondLevelLabels.WithName(GameObjects.DescriptionLevel2);
            thirdLevelDescription = thirdLevelLabels.WithName(GameObjects.DescriptionLevel3);
            fourthLevelDescription = fourthLevelLabels.WithName(GameObjects.DescriptionLevel4);
            fifthLevelDescription = fifthLevelLabels.WithName(GameObjects.DescriptionLevel5);

            firstLevelDescription.text = languageModel.Status[UNLOCKED_INDEX];
            secondLevelDescription.text = languageModel.Status[UNLOCKED_INDEX];
            thirdLevelDescription.text = languageModel.Status[UNLOCKED_INDEX];
            fourthLevelDescription.text = languageModel.Status[UNLOCKED_INDEX];
            fifthLevelDescription.text = languageModel.Status[UNLOCKED_INDEX];
        }

        private void SetTitlesToButtons(TextMeshProUGUI[] firstLevelLabels, 
                                        TextMeshProUGUI[] secondLevelLabels,
                                        TextMeshProUGUI[] thirdLevelLabels, 
                                        TextMeshProUGUI[] fourthLevelLabels, 
                                        TextMeshProUGUI[] fifthLevelLabels)
        {
            firstLevelTitle = firstLevelLabels.WithName(GameObjects.TitleLevel1);
            secondLevelTitle = secondLevelLabels.WithName(GameObjects.TitleLevel2);
            thirdLevelTitle = thirdLevelLabels.WithName(GameObjects.TitleLevel3);
            fourthLevelTitle = fourthLevelLabels.WithName(GameObjects.TitleLevel4);
            fifthLevelTitle = fifthLevelLabels.WithName(GameObjects.TitleLevel5);

            firstLevelTitle.text = languageModel.Names[0];
            secondLevelTitle.text = languageModel.Names[1];
            thirdLevelTitle.text = languageModel.Names[2];
            fourthLevelTitle.text = languageModel.Names[3];
            fifthLevelTitle.text = languageModel.Names[4];
        }

        // Author : Vivianne Lord
        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }

        // Author : Joanie Labbé
        private void UpdateFromUnlockLevel()
        {
            int lastLevelUnlocked = Finder.SaveFileSelection.LastLevelUnlocked;
            if (lastLevelUnlocked < 5)
            {
                fifthLevelImage.sprite = imageWhenLevelNotUnlocked;
                fifthLevelDescription.text = languageModel.Status[LOCKED_INDEX];
            }
            if (lastLevelUnlocked < 4)
            {
                fourthLevelImage.sprite = imageWhenLevelNotUnlocked;
                fourthLevelDescription.text = languageModel.Status[LOCKED_INDEX];
            }
            if (lastLevelUnlocked < 3)
            {
                thirdLevelImage.sprite = imageWhenLevelNotUnlocked;
                thirdLevelDescription.text = languageModel.Status[LOCKED_INDEX];
            }
            if (lastLevelUnlocked < 2)
            {
                secondLevelImage.sprite = imageWhenLevelNotUnlocked;
                secondLevelDescription.text = languageModel.Status[LOCKED_INDEX];
            }
        }

        private void SelectFirstLevel()
        {
            QuitToChosenLevel(1);
        }

        private void SelectSecondLevel()
        {
            if (secondLevelDescription.text != languageModel.Status[LOCKED_INDEX])
            {
                QuitToChosenLevel(2);
            }
        }

        private void SelectThirdLevel()
        {
            if (thirdLevelDescription.text != languageModel.Status[LOCKED_INDEX])
            {
                QuitToChosenLevel(3);
            }
        }
        
        private void SelectFourthLevel()
        {
            if (fourthLevelDescription.text != languageModel.Status[LOCKED_INDEX])
            {
                QuitToChosenLevel(4);
            }
        }
        
        private void SelectFifthLevel()
        {
            if (fifthLevelDescription.text != languageModel.Status[LOCKED_INDEX])
            {
                QuitToChosenLevel(5);
            }
        }

        private void ReturnToMainMenu()
        {
            menuNavigator.Pop();
            IEnumerator Routine()
            {
                yield return main.UnloadLevelSelectionScenes();
            }
            
            StartCoroutine(Routine());
        }

        private void QuitToChosenLevel(int chosenLevel)
        {
            main.LevelToLoad = chosenLevel;
            menuNavigator.InverseMenuOrder();

            IEnumerator Routine()
            {
                yield return main.LoadGameScenes();
                yield return main.UnloadHomeScenes();
                yield return main.UnloadLevelSelectionScenes();
            }

            StartCoroutine(Routine());
        }
        
        private void SelectButtonIfNoneSelected()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                backButton.Select();
            }
        }
        
        public void Show()
        {
            levelSelectionCanvas.enabled = true;
            backButton.Select();
        }

        public void Hide()
        {
            levelSelectionCanvas.enabled = false;
        }
    }
}