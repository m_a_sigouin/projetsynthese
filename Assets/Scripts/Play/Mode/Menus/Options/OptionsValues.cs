﻿using System;

namespace Game
{
    // Author : Kamylle Thériault
    [Serializable]
    public class OptionsValues
    {
        public float stepsVolumeValue;
        public float environmentPercentageValue;
        public bool fullScreenActivated;
        public Languages language;
    }
}