﻿using System;
using System.IO;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Audio;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.OptionsController)]
    public class OptionsController : MonoBehaviour
    {
        [field: SerializeField] [NotNull] public OptionsValues Values { get; set; }
        [SerializeField] private AudioMixer audioMixer;
        private const string ENIVRONMENT_PARAMETER = "environmentVolume";
        private const string STEP_PARAMETER = "stepsVolume";
        private const int LOG_CHANGER = 20;

        private const float MAX_VALUE = 1f;
        private const float MIN_VALUE = 0.001f;
        private const float TOLERANCE = 0.0001f;
        
        private OptionsMenu menu;
        
        private string savefilePath;

        private LanguageEventChannel languageEventChannel;

        private void Awake()
        {
            savefilePath = Application.persistentDataPath + "/OptionsData.json";
            if (File.Exists(savefilePath))
            {
                LoadOptionsValue();
                Screen.fullScreen = Values.fullScreenActivated;
            }
            else
            {
                FindSystemLanguage();
                SaveOptionsValue();
            }
            languageEventChannel = Finder.LanguageEventChannel;
        }

        private void Start()
        {
            SetInitialVolume();
        }

        public void InitializeController(OptionsMenu menu)
        {
            this.menu = menu;
        }

        public void SaveOptionsValue()
        {
            string optionsToString = JsonUtility.ToJson(Values);
            File.WriteAllText(savefilePath, optionsToString);
        }

        public void LoadOptionsValue()
        { 
            using (StreamReader reader = new StreamReader(savefilePath))
            {
                string jsonFile = reader.ReadToEnd();
                Values = JsonUtility.FromJson<OptionsValues>(jsonFile);
            } 
            
        }

        private void SetInitialVolume()
        {
            var temp = Mathf.Log(Values.stepsVolumeValue) * LOG_CHANGER;
            audioMixer.SetFloat(STEP_PARAMETER , Mathf.Log(Values.stepsVolumeValue)*LOG_CHANGER);
            audioMixer.SetFloat(ENIVRONMENT_PARAMETER, Mathf.Log(Values.environmentPercentageValue)*LOG_CHANGER);
        }

        public void GetInitialValues()
        {
            menu.SetInitialValues(Values);
        }
        
        public void SetStepVolumeValue(float value)
        {
            if (value >= MIN_VALUE && value <= MAX_VALUE && Math.Abs(value - Values.stepsVolumeValue) > TOLERANCE)
            {
                Values.stepsVolumeValue = value;
            }
        }

        public void SetEnvironmentVolumeValue(float value)
        {
            if (value >= MIN_VALUE && value <= MAX_VALUE && Math.Abs(value - Values.environmentPercentageValue) > TOLERANCE)
            {
                Values.environmentPercentageValue = value;
            }
        }

        public void SetFullScreenActivity(bool isActive)
        {
            Values.fullScreenActivated = isActive;
            Screen.fullScreen = isActive;
        }

        public void SetGameLanguage(Languages language)
        {
            Values.language = language;
            languageEventChannel.Publish();
        }
        
        private void FindSystemLanguage()
        {
            var systemLanguage = Application.systemLanguage;
            switch (systemLanguage)
            {
                case SystemLanguage.French:
                    Values.language = Languages.FR;
                    break;
                case SystemLanguage.Norwegian:
                    Values.language = Languages.NOR;
                    break;
                case SystemLanguage.English:
                    Values.language = Languages.ENG;
                    break;
                default:
                    Values.language = Languages.ENG;
                    break;
            }
        }
    }
}