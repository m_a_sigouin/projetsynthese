using System;
using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    public class HomeMenu : MonoBehaviour, IMenu
    {
        private Main main;
        private InputActions.MenuActions menuInputs;
        private MenuNavigator menuNavigator;
        private HomeLanguageModel languageModel;
        private LanguageEventChannel languageEventChannel;

        private TextMeshProUGUI homeLabel;
        private TextMeshProUGUI[] playButtonText;
        private TextMeshProUGUI[] quitButtonText;
        private TextMeshProUGUI[] selectLevelButtonText;
        private TextMeshProUGUI[] optionsButtonText;
        private TextMeshProUGUI[] achievementsButtonText;

        private Button playButton;
        private Button quitButton;
        private Button selectLevelButton;
        private Button optionsButton;
        private Button achievementsButton;
        private Button[] buttons;
        
        private Canvas homeCanvas;

        private void Awake()
        {
            main = Finder.Main;
            menuInputs = Finder.Inputs.Actions.Menu;
            menuNavigator = Finder.MenuNavigator;
            languageModel = new HomeLanguageModel();
            languageEventChannel = Finder.LanguageEventChannel;
            languageEventChannel.OnLanguageChangeEvent += ChangeActiveLanguage;
            
            homeCanvas = GetComponentInChildren<Canvas>();
            
            SetActiveLanguage();
            homeLabel = GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.Title);
            homeLabel.text = languageModel.Labels[0];
            FindButtonsInView();
        }

        private void Start()
        {
            menuInputs.Enable();
            playButton.Select();
            homeCanvas.enabled = true;
            menuNavigator.Push(this);
        }

        private void OnEnable()
        {
            playButton.onClick.AddListener(StartGame);
            quitButton.onClick.AddListener(QuitApplication);
            optionsButton.onClick.AddListener(OpenOptionsMenu);
            selectLevelButton.onClick.AddListener(OpenLevelSelectionMenu);
            achievementsButton.onClick.AddListener(OpenAchievementsMenu);
        }

        private void OnDisable()
        {
            playButton.onClick.RemoveListener(StartGame);
            quitButton.onClick.RemoveListener(QuitApplication);
            optionsButton.onClick.RemoveListener(OpenOptionsMenu);
            selectLevelButton.onClick.RemoveListener(OpenLevelSelectionMenu);
            achievementsButton.onClick.RemoveListener(OpenAchievementsMenu);
        }

        private void OnDestroy()
        {
            menuNavigator.Pop();
            languageEventChannel.OnLanguageChangeEvent -= ChangeActiveLanguage;
        }

        private void Update()
        {
            if (homeCanvas.enabled)
            {
                SelectButtonIfNoneSelected();
            }
        }

        private void FindButtonsInView()
        {
            buttons = GetComponentsInChildren<Button>();

            playButton = buttons.WithName(GameObjects.Play);
            quitButton = buttons.WithName(GameObjects.Quit);
            optionsButton = buttons.WithName(GameObjects.Options);
            selectLevelButton = buttons.WithName(GameObjects.SelectLevel);
            achievementsButton = buttons.WithName(GameObjects.Achievements);
            
            SetLabelsToButtons();
        }
        
        // Author : Kamylle Thériault, Vivianne Lord
        private void SetLabelsToButtons()
        {
            playButtonText = playButton.GetComponentsInChildren<TextMeshProUGUI>();
            quitButtonText = quitButton.GetComponentsInChildren<TextMeshProUGUI>();
            selectLevelButtonText = selectLevelButton.GetComponentsInChildren<TextMeshProUGUI>();
            optionsButtonText = optionsButton.GetComponentsInChildren<TextMeshProUGUI>();
            achievementsButtonText = achievementsButton.GetComponentsInChildren<TextMeshProUGUI>();

            for (int g = 0; g < playButtonText.Length; g++)
            {
                playButtonText[g].text = languageModel.Buttons[0];
                quitButtonText[g].text = languageModel.Buttons[3];
                selectLevelButtonText[g].text = languageModel.Buttons[1];
                optionsButtonText[g].text = languageModel.Buttons[2];
                achievementsButtonText[g].text = languageModel.Buttons[4];
            }
        }

        // Author : Vivianne Lord
        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }

        private void ChangeActiveLanguage()
        {
            SetActiveLanguage();
            
            for (int g = 0; g < playButtonText.Length; g++)
            {
                playButtonText[g].text = languageModel.Buttons[0];
                quitButtonText[g].text = languageModel.Buttons[3];
                selectLevelButtonText[g].text = languageModel.Buttons[1];
                optionsButtonText[g].text = languageModel.Buttons[2];
                achievementsButtonText[g].text = languageModel.Buttons[4];
            }
            homeLabel.text = languageModel.Labels[0];
        }

        private void StartGame()
        {
            main.LevelToLoad = 1;
            
            IEnumerator Routine()
            {
                yield return main.LoadGameScenes();
                yield return main.UnloadHomeScenes();
            }

            StartCoroutine(Routine());
        }

        private void QuitApplication()
        {
            Finder.SaveFileSelection.SaveFileValue();
            ApplicationExtensions.Quit();
        }
        
        // Author : Kamylle Thériault
        private void OpenOptionsMenu()
        {
            IEnumerator Routine()
            {
                yield return main.LoadOptionsScenes();
            }
            
            StartCoroutine(Routine());
        }
        
        // Author : Kamylle Thériault
        private void OpenLevelSelectionMenu()
        {
            IEnumerator Routine()
            {
                yield return main.LoadLevelSelectionScenes();
            }
            
            StartCoroutine(Routine());
        }
        
        // Author : Kamylle Thériault
        private void OpenAchievementsMenu()
        {
            IEnumerator Routine()
            {
                yield return main.LoadAchievementsMenuScenes();
            }

            StartCoroutine(Routine());
        }
        
        // Author : Kamylle Thériault
        private void SelectButtonIfNoneSelected()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                playButton.Select();
            }
        }

        // Author : Kamylle Thériault
        public void Show()
        {
            homeCanvas.enabled = true;
            SetButtonsActive(true);
            playButton.Select();
        }

        // Author : Kamylle Thériault
        public void Hide()
        {
            homeCanvas.enabled = false;
            SetButtonsActive(false);
        }

        // Author : Joanie Labbe
        private void SetButtonsActive(bool enabled)
        {
            foreach (var button in buttons)
            {
                button.enabled = enabled;
            }
        }
        
    }
}