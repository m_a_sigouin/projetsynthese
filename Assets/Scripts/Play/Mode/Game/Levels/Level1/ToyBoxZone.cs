﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class ToyBoxZone : MonoBehaviour
    {
        [SerializeField] private List<GameObject> requiredDogToys;
        [Header("Sound")] 
        [SerializeField] private AudioClip toyNoise;

        private List<GameObject> toysInBox;
        private AchievementPublisher achievementPublisher;
        private SoundPlayer soundPlayer;

        private void Awake()
        {
            toysInBox = new List<GameObject>();
            achievementPublisher = Finder.AchievementPublisher;
            soundPlayer = Finder.SoundPlayer;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Rope Toy"
                || other.name == "Duck Plush"
                || other.name == "Ball"
                || other.name == "Bone")
            {
                toysInBox.Add(other.gameObject);
                soundPlayer.PlaySound(toyNoise);
                other.gameObject.SetActive(false);
                AllToysCollected();
            }
        }

        private void AllToysCollected()
        {
            if (toysInBox.Count == requiredDogToys.Count)
            {
                achievementPublisher.UnlockAchievement(AchievementType.Level1PutAllDogToysInBox);
            }
        }
    }
}