﻿using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class HUDTimerModel
    {
        private float timeLimit;
        public float TimeLeftInSeconds { get; private set; }

        public HUDTimerModel(float timeLimit)
        {
            this.timeLimit = timeLimit;
            RestartTimer();
        }

        public void ReduceTime()
        {
            TimeLeftInSeconds -= Time.deltaTime;
        }
        
        public void RestartTimer()
        {
            TimeLeftInSeconds = timeLimit;
        }
        
        public string GetFormattedTime()
        {
            string minutes = Mathf.Floor(TimeLeftInSeconds / 60).ToString("00");
            string seconds = Mathf.Floor(TimeLeftInSeconds % 60).ToString("00");
            return $"{minutes}:{seconds}";
        }
    }
}