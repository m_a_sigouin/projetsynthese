﻿using System;
using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.HUD)]
    public class HUDTimerController : MonoBehaviour
    {
        [SerializeField][Range(0, 1000)] private float timeLimit = 300f;

        private int currentLevel;
        private HUDTimerModel hudTimerModel;
        private HUDTimerView hudTimerView;
        private FlagUpdates flagUpdates;
        private LevelFailedEventChannel levelFailedEventChannel;

        private void Awake()
        {
            hudTimerModel = new HUDTimerModel(timeLimit);
            hudTimerView = new HUDTimerView();
            flagUpdates = Finder.FlagUpdates;

            levelFailedEventChannel = Finder.LevelFailedEventChannel;
            levelFailedEventChannel.OnLevelFailed += OnLevelFailed;

            hudTimerView.TimerText = GameObject.FindWithTag(Tags.Timer).GetComponent<TMP_Text>();
            hudTimerView.UpdateState(false);
        }

        private void OnDestroy()
        {
            levelFailedEventChannel.OnLevelFailed -= OnLevelFailed;
        }

        private void OnLevelFailed()
        {
            if (currentLevel == 3)
            {
                hudTimerModel.RestartTimer();
            }
        }

        private void Update()
        {
            if (currentLevel == 3 && VerifyEligibility() && flagUpdates.PlayerShouldUpdate)
            {
                hudTimerModel.ReduceTime();
                hudTimerView.UpdateTimerText(hudTimerModel.GetFormattedTime());
            }
        }

        public void UpdateState(int level)
        {
            currentLevel = level;
            hudTimerView.UpdateState(level == 3);
        }

        public bool VerifyEligibility()
        {
            return hudTimerModel.TimeLeftInSeconds - Time.deltaTime > 0;
        }
    }
}