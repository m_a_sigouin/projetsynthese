﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.HUD)]
    public class HUDConcentrationLevel : MonoBehaviour
    {
        private Slider concentrationLevelSlider;
        private ConcentrationEventChannel eventChannel;
        
        public float ConcentrationLevel
        {
            set => concentrationLevelSlider.value = value;
        }
        
        private void Awake()
        {
            concentrationLevelSlider = GetComponentInChildren<Slider>();
            eventChannel = Finder.ConcentrationEventChannel;
            eventChannel.OnConcentrationDepleted += ChangeSliderEnabled;
        }

        private void OnDestroy()
        {
            eventChannel.OnConcentrationDepleted -= ChangeSliderEnabled;
        }
        
        private void ChangeSliderEnabled(bool isEmpty)
        {
            concentrationLevelSlider.enabled = !isEmpty;
        }
    }
}