﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class SpecificTimeEnemySpawner : EnemySpawner
    {
        [SerializeField] protected float timeToWaitInSecond = 3f;
        
        private FlagUpdates flag;
        
        private double dateTimeSinceLastSpawn;
        
        private void Awake()
        {
            flag = Finder.FlagUpdates;
            dateTimeSinceLastSpawn = Time.time;
            shouldSpawn = true;
        }

        private void Update()
        {
            if (flag.SpawnersShouldUpdate)
            {
                if (Time.time - dateTimeSinceLastSpawn >= timeToWaitInSecond)
                {
                    ResetCooldown();
                }
                if (shouldSpawn)
                {
                    SpawnEntity();
                }
            }
        }
        
        private void ResetCooldown()
        {
            shouldSpawn = true;
            dateTimeSinceLastSpawn = Time.time;
        }
    }
}