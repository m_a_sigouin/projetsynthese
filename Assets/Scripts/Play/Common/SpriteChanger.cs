﻿using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class SpriteChanger : MonoBehaviour
    {
        [SerializeField] private Sprite objectSpriteDown;
        [SerializeField] private Sprite objectSpriteUp;
        [SerializeField] private Sprite objectSpriteLeft;
        [SerializeField] private Sprite objectSpriteRight;
        [SerializeField] private Sprite objectSpriteIdle;

        private bool isVehicle;

        private const float DIRECTION_THRESHOLD = 0.06f;
        private SpriteRenderer objectSprite;

        private void Awake()
        {
            objectSprite = GetComponent<SpriteRenderer>();
            if (objectSprite == null)
            {
                objectSprite = GetComponentInChildren<SpriteRenderer>();
            }

            isVehicle = GetComponent<Vehicle>() != null;
        }

        public void DetermineSpriteToUse (Vector3 direction)
        {
            if (direction.x < -DIRECTION_THRESHOLD)
            {
                ChangeSprite(objectSpriteLeft);
                if (objectSpriteLeft == objectSpriteRight && !objectSprite.flipX)
                {
                    objectSprite.flipX = true;
                }
            }
            else if (direction.x > DIRECTION_THRESHOLD)
            {
                ChangeSprite(objectSpriteRight);
                if (objectSpriteLeft == objectSpriteRight && objectSprite.flipX)
                {
                    objectSprite.flipX = false;
                }
            }
            else if (direction.y < 0)
            {
                ChangeSprite(objectSpriteDown);
            }
            else if (direction.y > 0)
            {
                ChangeSprite(objectSpriteUp);
            }
            else if (direction == Vector3.zero)
            {
                ChangeSprite(objectSpriteIdle);
            }

            if (isVehicle)
            {
                transform.up = Vector3.Lerp(transform.up, direction, Time.deltaTime * 2);
            }
        }
        
        private void ChangeSprite (Sprite newSprite)
        {
            if (newSprite != null && objectSprite != null && objectSprite.sprite != newSprite)
            {
                objectSprite.sprite = newSprite;
            }
        }
    }
}