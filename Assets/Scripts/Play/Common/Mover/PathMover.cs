﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    // L'inspiration provient de https://gist.github.com/Abban/42721b25cebba33c389a
    public class PathMover : MonoBehaviour
    {
        [SerializeField] private float speed = 2f;
        [SerializeField] private List<Waypoint> waypoints;
        [SerializeField] private bool repeat;
        [SerializeField] private bool shouldRotate = true;
        [SerializeField] private float rotationSpeed = 2f;
        [SerializeField] private float tolerance = 0.1f;
        
        public event HasFinishPath OnPathFinish;
        
        private SpriteChanger spriteChanger;
        private new Rigidbody2D rigidbody2D;
        private Waypoint currentWaypoint;
        
        private int currentIndex;
        private bool isDelayStarted;
        private bool isWaiting;
        private float delayRemaining;

        public List<Waypoint> Waypoints
        {
            get => waypoints;
        }

        public bool Repeat
        {
            set => repeat = value;
        }

        public float Speed => speed;

        public Waypoint Waypoint => currentWaypoint;

        public bool IsPaused => isWaiting;

        private void Awake()
        {
            spriteChanger = GetComponent<SpriteChanger>();
            CreateRigidBody();
            SetPath();
            isDelayStarted = false;
            if (waypoints == null)
            {
                waypoints = new List<Waypoint>();
            }
        }

        public void SetPath()
        {
            currentIndex = 0;
            if (waypoints != null && waypoints.Count > 0)
            {
                currentWaypoint = waypoints[currentIndex];
                SetAwakeRotation();
            }
        }

        public void Move()
        {
            if (currentWaypoint != null && !isWaiting)
            {
                MoveTowardWaypoints();
            }
            else if (isWaiting)
            {
                Delay();
            }
        }

        private void MoveTowardWaypoints()
        {
            Vector2 currentPosition = transform.position;
            Vector2 targetPosition = currentWaypoint.transform.position;
            
            // Approximation pour éviter un bouchon de circulation et des problèmes d'affichage
            if (Vector2.Distance(currentPosition, targetPosition) > tolerance)
            {
                Vector2 movement = targetPosition - currentPosition;
                movement.Normalize();
                movement *= speed * Time.deltaTime;
                
                // Il faut spécifier que le mouvement se fait par rapport à sa position dans le monde sinon
                // il va se fier à sa rotation et bouger dans tous les sens en essayant de se rendre à sa destination.
                transform.Translate(movement, Space.World);
                Rotate();
            }
            else
            {
                if (currentWaypoint.IsWaitPoint)
                {
                    Delay();
                    Rotate();
                }
                else
                {
                    NextWaypoint();
                }
            }
        }

        private void NextWaypoint ()
        {
            if (currentIndex < waypoints.Count - 1)
            {
                currentIndex += 1;
            }
            else if (currentIndex >= waypoints.Count - 1)
            {
                if (repeat)
                {
                    currentIndex = 0;
                }
                if (OnPathFinish != null && !isWaiting)
                {
                    OnPathFinish();
                }
            }
            
            currentWaypoint = waypoints[currentIndex];
        }

        public void Pause(bool isPaused)
        {
            isWaiting = isPaused;
        }

        // Author : Marc-Antoine Sigouin
        private void Rotate()
        {
            if (shouldRotate)
            {
                Vector3 direction = currentWaypoint.transform.position - transform.position;
                if (spriteChanger == null)
                {
                    transform.up = Vector3.Lerp(transform.up, direction, Time.deltaTime * rotationSpeed);
                }
                else
                {
                    if (direction.magnitude > tolerance)
                    {
                        spriteChanger.DetermineSpriteToUse(direction);
                    }
                    else
                    {
                        spriteChanger.DetermineSpriteToUse(Vector3.zero);
                    }
                }
            }
        }

        private void SetAwakeRotation()
        {
            if (shouldRotate)
            {
                Vector3 direction = currentWaypoint.transform.position - transform.position;
                if (spriteChanger == null)
                {
                    transform.up = direction;
                }
                else
                {
                    spriteChanger.DetermineSpriteToUse(direction);
                }
            }
        }

        // Author : Marc-Antoine Sigouin
        private void Delay()
        {
            if (!isDelayStarted)
            {
                delayRemaining = currentWaypoint.WaitTime;
                isDelayStarted = true;
                Pause(true);
            }
            
            delayRemaining -= Time.deltaTime;

            if (delayRemaining <= 0 && currentWaypoint.WaitTime > 0)
            {
                NextWaypoint();
                isDelayStarted = false;
                Pause(false);
            }
        }

        private void CreateRigidBody()
        {
            rigidbody2D = gameObject.AddComponent<Rigidbody2D>();
            rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
            rigidbody2D.useFullKinematicContacts = true;
        }
    }
    
    public delegate void HasFinishPath();
}