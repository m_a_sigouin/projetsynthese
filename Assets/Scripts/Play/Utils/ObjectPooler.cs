﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    // Référence pour le pool d'objet
    // https://www.raywenderlich.com/847-object-pooling-in-unity#toc-anchor-001

    // Author : Joanie Labbé
    [Findable(Tags.LevelController)]
    public class ObjectPooler : MonoBehaviour
    {
        [SerializeField] private List<GameObject> pooledObjects;
        [SerializeField] private List<ObjectPoolItem> itemsToPool;

        private void Awake()
        {
            pooledObjects = new List<GameObject>();
            foreach (ObjectPoolItem item in itemsToPool)
            {
                for (int i = 0; i < item.amountToPool; i++)
                {
                    InstantiateObject(item);
                }
            }
        }

        private GameObject GetPooledObject(string tagObjectToGet)
        {
            for (int i = 0; i < pooledObjects.Count; i++)
            {
                if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].CompareTag(tagObjectToGet))
                {
                    return pooledObjects[i];
                }
            }
            foreach (ObjectPoolItem item in itemsToPool)
            {
                if (item.objectToPool.CompareTag(tagObjectToGet))
                {
                    if (item.shouldExpand)
                    {
                        return InstantiateObject(item);
                    }
                }
            }
            return null;
        }

        private GameObject InstantiateObject(ObjectPoolItem item)
        {
            GameObject obj = Instantiate(item.objectToPool);
            obj.SetActive(false);
            SceneManager.MoveGameObjectToScene(obj, gameObject.scene);
            pooledObjects.Add(obj);
            return obj;
        }

        public static GameObject EnableObject(string tagObjectToGet, Vector3 position, Quaternion rotation)
        {
            GameObject obj = Finder.ObjectPooler.GetPooledObject(tagObjectToGet);
            if (obj != null)
            {
                obj.transform.position = position;
                obj.transform.rotation = rotation;
                obj.SetActive(true);
            }

            return obj;
        }
        
        [Serializable]
        public class ObjectPoolItem
        {
            public GameObject objectToPool;
            public int amountToPool;
            public bool shouldExpand;
        }
    }
}