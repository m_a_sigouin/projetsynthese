﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class Recyclable : MonoBehaviour
    {
        protected void Recycle()
        {
            gameObject.SetActive(false);
        }
    }
}