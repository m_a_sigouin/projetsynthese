﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public static class BooleanExtension
    {
        public static IEnumerator ChangeState(this bool boolToChangeState, float delay)
        {
            IEnumerator ChangeStateRoutine()
            {
                boolToChangeState = false;
                yield return new WaitForSeconds(delay);
                boolToChangeState = true;
            }

            return ChangeStateRoutine();
        }
    }
}