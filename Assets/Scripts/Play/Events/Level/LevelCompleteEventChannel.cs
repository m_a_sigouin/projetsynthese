﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    [Findable(Tags.MainController)]
    public class LevelCompleteEventChannel : MonoBehaviour
    {
        public event LevelCompleteEvent OnLevelComplete;

        public void Publish()
        {
            if (OnLevelComplete != null)
            {
                Finder.AchievementPublisher.UnlockAchievement(AchievementType.LevelCompleted);
                OnLevelComplete();
            }
        }
    }

    public delegate void LevelCompleteEvent();
}