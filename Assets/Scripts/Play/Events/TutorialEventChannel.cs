﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    [Findable(Tags.MainController)]
    public class TutorialEventChannel : MonoBehaviour
    {
        public event TutorialPromptDisplayEvent OnPromptDisplay;
        public event TutorialPromptMaskingEvent OnPromptMasking;

        public void PublishPrompt(string zoneName)
        {
            if (OnPromptDisplay != null)
            {
                OnPromptDisplay(zoneName);
            }
        }

        public void PublishMasking()
        {
            if (OnPromptMasking != null)
            {
                OnPromptMasking();
            }
        }
    }

    public delegate void TutorialPromptDisplayEvent(string tutorialZone);

    public delegate void TutorialPromptMaskingEvent();
}